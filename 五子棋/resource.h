//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Five.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_FIVETYPE                    129
#define IDC_CURSOR1                     130
#define IDC_CURSOR2                     131
#define IDB_BITMAP_BLACK                132
#define IDB_BITMAP_WHITE                133
#define IDI_ICON_BLACK                  134
#define IDI_ICON_WHITE                  135
#define IDB_BITMAP1                     136
#define IDB_BITMAP2                     137
#define ID_START                        32771
#define ID_SAVE                         32772
#define ID_OPEN                         32773
#define ID_VSCPUF                       32774
#define ID_VSCPUS                       32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
