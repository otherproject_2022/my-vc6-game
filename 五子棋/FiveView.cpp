// FiveView.cpp : implementation of the CFiveView class
//

#include "stdafx.h"
#include "Five.h"

#include "math.h"

#include "FiveDoc.h"
#include "FiveView.h"
#include "MainFrm.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFiveView

IMPLEMENT_DYNCREATE(CFiveView, CView)

BEGIN_MESSAGE_MAP(CFiveView, CView)
	//{{AFX_MSG_MAP(CFiveView)
	ON_WM_LBUTTONUP()
	ON_COMMAND(ID_OPEN, OnOpen)
	ON_COMMAND(ID_SAVE, OnSave)
	ON_COMMAND(ID_START, OnStart)
	ON_WM_SETCURSOR()
	ON_COMMAND(ID_VSCPUF, OnVscpuf)
	ON_COMMAND(ID_VSCPUS, OnVscpus)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFiveView construction/destruction

CFiveView::CFiveView()
{
	// TODO: add construction code here
	hcursorblack=AfxGetApp()->LoadCursor(IDC_CURSOR1);
	hcursorwhite=AfxGetApp()->LoadCursor(IDC_CURSOR2);
	m_bmpwhite.LoadBitmap(IDB_BITMAP_WHITE);
	m_bmpblack.LoadBitmap(IDB_BITMAP_BLACK);
	for (int i=0;i<19;i++)
	{
		for (int j=0;j<19;j++)
		{
			wzq[i][j]=0;
		}

	}
	colorwhite=true;

}

CFiveView::~CFiveView()
{
}

BOOL CFiveView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFiveView drawing

void CFiveView::OnDraw(CDC* pDC)
{
	CFiveDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CBrush mybrush1;
	mybrush1.CreateSolidBrush(RGB(192,191,192));
	CRect myrect1(0,0,1200,800);
	pDC->FillRect(myrect1,&mybrush1);

	CPen mypen;
	CPen*myoldpen;
	mypen.CreatePen(PS_SOLID,1,RGB(0,0,0));
	myoldpen=pDC->SelectObject(&mypen);
	for(int i=0;i<19;i++)
	{
		pDC->MoveTo(40,40+i*20);
		pDC->LineTo(400,40+i*20);
		pDC->MoveTo(40+i*20,40);
		pDC->LineTo(40+i*20,400);
	}
	CDC Dc;
	Dc.CreateCompatibleDC(pDC);
	for(int n=0;n<19;n++)
		for (int m=0;m<19;m++)
		{
			if (wzq[n][m]==1)
			{
				Dc.SelectObject(m_bmpwhite);
				pDC->BitBlt(n*20+32,m*20+32,160,160,&Dc,0,0,SRCCOPY);
			}
			if (wzq[n][m]==-1)
			{
				Dc.SelectObject(m_bmpblack);
				pDC->BitBlt(n*20+32,m*20+32,160,160,&Dc,0,0,SRCCOPY);
			}
		}
}

/////////////////////////////////////////////////////////////////////////////
// CFiveView printing

BOOL CFiveView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFiveView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFiveView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CFiveView diagnostics

#ifdef _DEBUG
void CFiveView::AssertValid() const
{
	CView::AssertValid();
}

void CFiveView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFiveDoc* CFiveView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFiveDoc)));
	return (CFiveDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFiveView message handlers



void CFiveView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CDC *pDC=GetDC();
	CDC Dc;
	if (point.x>30&&point.x<410&&point.y>30&&point.y<410)
	{
		int px=(point.x-30)/20;
		int py=(point.y-30)/20;

		
		if (wzq[px][py]!=0)
			return;
		putdown(px,py);
	
	}
	CView::OnLButtonUp(nFlags, point);
}

void CFiveView::OnOpen() 
{
	// TODO: Add your command handler code here
	CFileDialog dlg(TRUE,"wzq",NULL,OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"(*.WZQ)|*.wzq||",this);
	if (dlg.DoModal()==IDOK)
		dlg.GetFileName();
	else
		return;
	CString str;
	int i,j,m;
	CStdioFile file;
	if (file.Open(dlg.GetFileName(),CFile::modeRead)==0)
	{
		AfxMessageBox("open error");
		return;
	}
	CArchive ar(&file,CArchive::load);
	for (i=0;i<19;i++)
	for(j=0;j<19;j++)
	{
		ar.ReadString(str);
		sscanf(str,"%d",&m);
		wzq[i][j]=m;
	}
	ar.ReadString(str);
	sscanf(str,"%d",&m);
	if (m==1)
	colorwhite=true;
	else
		colorwhite=false;
	file.Close();
	ar.Close();
	Invalidate();
	
}

void CFiveView::OnSave() 
{
	// TODO: Add your command handler code here
	CFileDialog dlg(FALSE,"wzq",NULL,OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"(*.WZQ)|*.wzq||",this);
	if (dlg.DoModal()==IDOK)
	{
		dlg.GetFileName();
	}
	else
		return;
	CString str;
	int i,j;

	CStdioFile file;
	if (file.Open(dlg.GetFileName(),CFile::modeCreate|CFile::modeWrite|CFile::typeText)==0)
	{
		AfxMessageBox("Save Error!");
		return;
	}
	for (i=0;i<19;i++)
		for (j=0;j<19;j++)
		{
			if (wzq[i][j]==-1)
				file.WriteString("-1\n");
			if (wzq[i][j]==0)
				file.WriteString("0\n");			
			if (wzq[i][j]==1)
				file.WriteString("1\n");					
		}
		if (colorwhite==true)
			file.WriteString("1\n");
		else
			file.WriteString("0\n");
		file.Close();
}

void CFiveView::OnStart() 
{
	// TODO: Add your command handler code here
	for (int i=0;i<19;i++)
	{
		for (int j=0;j<19;j++)
		{
			wzq[i][j]=0;
		}
		
	}
	colorwhite=false;
	vscup=0;
	Invalidate();
	
}

void CFiveView::Save()
{

}

void CFiveView::over(int x,int y)
{

	int xmin=(x>4)?4:x;
	int xmax=(18-x)>4?4:18-x;
	int ymin=(y>4)?4:y;
	int ymax=(18-y)>4?4:18-y;
	int sum;
	int i,j;
	CString str;
	for (i=x-xmin;i<=x+xmax-4;i++)
	{
		sum=0;
		for (j=i;j<i+5;j++)
		{
			sum+=wzq[j][y];
		}
		if (sum==5)
		{
			AfxMessageBox("����ʤ");
			return;
		}
		if (sum==-5)
		{
			AfxMessageBox("����ʤ");
			return;
		}
	}

	for (i=y-ymin;i<=y+ymax-4;i++)
	{
		sum=0;
		for (j=i;j<i+5;j++)
		{
			sum+=wzq[x][j];
		}
		if (sum==5)
		{
			AfxMessageBox("����ʤ");
			return;
		}
		if (sum==-5)
		{
			AfxMessageBox("����ʤ");
			return;
		}
	}
	int xymin=xmin<ymin?xmin:ymin;
	int xymax=xmax<ymax?xmax:ymax;
	for (i=0-xymin;i<=xymax-4;i++)
	{
		sum=0;
		for (j=i;j<i+5;j++)
		{
			sum+=wzq[x+j][y+j];
		}
		if (sum==5)
		{
			AfxMessageBox("����ʤ");
			return;
		}
		if (sum==-5)
		{
			AfxMessageBox("����ʤ");
			return;
		}
	}

		xymin=xmin<ymax?xmin:ymax;
		xymax=xmax<ymin?xmax:ymin;
	for (i=0-xymin;i<=xymax-4;i++)
	{
		sum=0;
		for (j=i;j<i+5;j++)
		{
			sum+=wzq[x+j][y-j];
		}
		if (sum==5)
		{
			AfxMessageBox("����ʤ");
			return;
		}
		if (sum==-5)
		{
			AfxMessageBox("����ʤ");
			return;
		}
	}

}

BOOL CFiveView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// TODO: Add your message handler code here and/or call default
	CMainFrame*pFrm=(CMainFrame*)AfxGetApp()->m_pMainWnd;
	CStatusBar*pStatus=&pFrm->m_wndStatusBar;
	if (colorwhite)
	{
		
		if (pStatus)
		{
			pStatus->GetStatusBarCtrl().SetIcon(0,AfxGetApp()->LoadIcon(IDI_ICON_WHITE));
			pStatus->SetPaneText(0,"������");
		}
		SetCursor(hcursorwhite);
	}
	else
	{
		if (pStatus)
		{
			pStatus->GetStatusBarCtrl().SetIcon(0,AfxGetApp()->LoadIcon(IDI_ICON_BLACK));
			pStatus->SetPaneText(0,"������");
		}
		SetCursor(hcursorblack);
	}
	return CView::OnSetCursor(pWnd, nHitTest, message);
}

void CFiveView::OnVscpuf() 
{
	// TODO: Add your command handler code here
	for (int i=0;i<19;i++)
	{
		for (int j=0;j<19;j++)
		{
			wzq[i][j]=0;
		}
		
	}
	colorwhite=false;
	vscup=1;
	computdown=true;
	Invalidate();
	putdown(9,9);
	
}

void CFiveView::OnVscpus() 
{
	// TODO: Add your command handler code here
	for (int i=0;i<19;i++)
	{
		for (int j=0;j<19;j++)
		{
			wzq[i][j]=0;
		}
		
	}
	comlastpoint.x=9;
	comlastpoint.y=9;
	colorwhite=false;
	vscup=2;
	Invalidate();	
}

void CFiveView::putdown(int x, int y)
{
	CRect myRect;
	myRect.left=x*20+32;
	myRect.right=myRect.left+20;
	myRect.top=y*20+32;
	myRect.bottom=myRect.top+20;
	if (colorwhite)
	{
		wzq[x][y]=1;
		InvalidateRect(&myRect);
		over(x,y);
		colorwhite=FALSE;
	}
	else
	{
		wzq[x][y]=-1;
		InvalidateRect(&myRect);
		over(x,y);
		colorwhite=TRUE;
	}
	InvalidateRect(&myRect);

	if (vscup!=0)
	{	computdown=!computdown;
		if (computdown)
		{	playerlastpoint.x=x;
			playerlastpoint.y=y;
			cpudown();
		}
		else
		{
			comlastpoint.x=x;
			comlastpoint.y=y;
		}
	}

}

void CFiveView::cpudown()
{
	CPoint att;
	CPoint def;
	CPoint temp;
	int att_leave;
	int def_leave;
	att_leave=comattputdown(att);
	def_leave=comdefdown(def);
	if (att_leave==10)
	{
		comlastpoint=att;
	}
	else
	{
		if (att_leave-1>def_leave)
			comlastpoint=att;
		else
		{
			comlastpoint=def;
			if (def_leave<=2)
			{
				
				temp=SearchNear();
				if (temp.x!=-1)
				{
					comlastpoint=temp;
				}
			}
		}
	}
	putdown(comlastpoint.x,comlastpoint.y);
}

int CFiveView::comattputdown(CPoint &m_point)
{
	int leave=0,maxleave=0;
	CPoint temp;
	int xmin=(comlastpoint.x>4)?4:comlastpoint.x;
	int xmax=(18-comlastpoint.x)>4?4:18-comlastpoint.x;
	int ymin=(comlastpoint.y>4)?4:comlastpoint.y;
	int ymax=(18-comlastpoint.y)>4?4:18-comlastpoint.y;
	int i,j;
	for (i=comlastpoint.x-xmin;i<=comlastpoint.x+xmax;i++)
		for (j=comlastpoint.y-ymin;j<=comlastpoint.y+ymax;j++)
		{
			if (wzq[i][j]==0)
			{
				if (colorwhite)
				leave=whiteputdown(i,j);
				else
				leave=blackputdown(i,j);
				if (leave>maxleave)
				{
					maxleave=leave;
					temp.x=i;
					temp.y=j;
				}
				if (leave==maxleave)
				{
				//	maxleave=leave;
					if (fabs(temp.x-i)<=1.1&&fabs(temp.y-j)<=1.1)
					{
						temp.x=i;
					temp.y=j;
					}
				
				}
			}
		}
	m_point=temp;
	return maxleave;
}

int CFiveView::comdefdown(CPoint &m_point)
{
	int leave=0,maxleave=0;
	CPoint temp;
	int xmin=(comlastpoint.x>4)?4:comlastpoint.x;
	int xmax=(18-comlastpoint.x)>4?4:18-comlastpoint.x;
	int ymin=(comlastpoint.y>4)?4:comlastpoint.y;
	int ymax=(18-comlastpoint.y)>4?4:18-comlastpoint.y;
	int i,j;
	for (i=comlastpoint.x-xmin;i<=comlastpoint.x+xmax;i++)
		for (j=comlastpoint.y-ymin;j<=comlastpoint.y+ymax;j++)
		{
			if (wzq[i][j]==0)
			{
				if (!colorwhite)
					leave=whiteputdown(i,j);
				else
					leave=blackputdown(i,j);
				if (leave>maxleave)
				{
					maxleave=leave;
					temp.x=i;
					temp.y=j;
				}
				if (leave==maxleave)
				{
					//	maxleave=leave;
					if (fabs(temp.x-i)<=1.1&&fabs(temp.y-j)<=1.1)
					{
						temp.x=i;
						temp.y=j;
					}
					
				}

			}
		}
		m_point=temp;
	return maxleave;
}

int CFiveView::blackputdown(int x, int y)
{
	int xmin=(x>4)?4:x;
	int xmax=(18-x)>4?4:18-x;
	int ymin=(y>4)?4:y;
	int ymax=(18-y)>4?4:18-y;
	int sum[4]={0};
	int temp;
	int i,j;
	for (i=x-xmin;i<=x+xmax-4;i++)
	{
		temp=0;
		for (j=i;j<i+5;j++)
		{
			temp+=wzq[j][y];
		}
		sum[0]=min(sum[0],temp);
	}
	
	for (i=y-ymin;i<=y+ymax-4;i++)
	{
		temp=0;
		for (j=i;j<i+5;j++)
		{
			temp+=wzq[x][j];
		}
		sum[1]=min(sum[1],temp);
	}
	int xymin=xmin<ymin?xmin:ymin;
	int xymax=xmax<ymax?xmax:ymax;
	for (i=0-xymin;i<=xymax-4;i++)
	{
		temp=0;
		for (j=i;j<i+5;j++)
		{
			temp+=wzq[x+j][y+j];
		}
		sum[2]=min(sum[2],temp);
	}	
	xymin=xmin<ymax?xmin:ymax;
	xymax=xmax<ymin?xmax:ymin;
	for (i=0-xymin;i<=xymax-4;i++)
	{
		temp=0;
		for (j=i;j<i+5;j++)
		{
			temp+=wzq[x+j][y-j];
		}
		sum[3]=min(sum[3],temp);
	}
	sum[0]--;
	sum[1]--;
	sum[2]--;
	sum[3]--;
	sum[0]=0-sum[0];
	sum[1]=0-sum[1];
	sum[2]=0-sum[2];
	sum[3]=0-sum[3];
	return checkleave(sum);

}

int CFiveView::whiteputdown(int x, int y)
{
	int xmin=(x>4)?4:x;
	int xmax=(18-x)>4?4:18-x;
	int ymin=(y>4)?4:y;
	int ymax=(18-y)>4?4:18-y;
	int sum[4]={0};
	int temp;
	int i,j;
	for (i=x-xmin;i<=x+xmax-4;i++)
	{
		temp=0;
		for (j=i;j<i+5;j++)
		{
			temp+=wzq[j][y];
		}
		sum[0]=max(sum[0],temp);
	}
	
	for (i=y-ymin;i<=y+ymax-4;i++)
	{
		temp=0;
		for (j=i;j<i+5;j++)
		{
			temp+=wzq[x][j];
		}
		sum[1]=max(sum[1],temp);
	}
	int xymin=xmin<ymin?xmin:ymin;
	int xymax=xmax<ymax?xmax:ymax;
	for (i=0-xymin;i<=xymax-4;i++)
	{
		temp=0;
		for (j=i;j<i+5;j++)
		{
			temp+=wzq[x+j][y+j];
		}
		sum[2]=max(sum[2],temp);
	}	
	xymin=xmin<ymax?xmin:ymax;
	xymax=xmax<ymin?xmax:ymin;
	for (i=0-xymin;i<=xymax-4;i++)
	{
		temp=0;
		for (j=i;j<i+5;j++)
		{
			temp+=wzq[x+j][y-j];
		}
		sum[3]=max(sum[3],temp);
	}
	sum[0]++;
	sum[1]++;
	sum[2]++;
	sum[3]++;
	
	return checkleave(sum);
}

int CFiveView::checkleave(int sum[])
{
	int max,max2;
	int i,maxi;
	max=sum[0];
	maxi=0;
	for (i=1;i<4;i++)
	{
		if (sum[i]>max)
		{
			max=sum[i];
			maxi=i;
		}			
	}
	sum[maxi]=sum[3];
	max2=sum[0];
	for (i=1;i<3;i++)
	{
		if (sum[i]>max2)
		{
			max2=sum[i];
		}			
	}
	if (max==5)
	{
		return 10;
	}
	if (max==4)
	{
		if (max2==4)
			return 9;
		if(max2==3)
			return 8;
		return 6;
	}
	if (max==3)
	{
		if (max2==3)
			return 7;
		if (max2==2)
			return 5;
			return 4;
	}
	if (max==2)
	{
		if (max2==2)
			return 3;
		return 2;
	}
	return 1;
}
///10��
///9˫��
///8����
///7˫��
///6��
///5����
///4��
///3����
///2��
///1һ

CPoint CFiveView::SearchNear()
{
	CPoint m_point;
	m_point.x=-1;
	m_point.y=-1;
	int x,y;
	for(x=playerlastpoint.x-1;x<=playerlastpoint.x+1;x++)
		for (y=playerlastpoint.y-1;y<=playerlastpoint.y+1;y++)
		{
			if (x>=0&&x<19&&y>=0&&y<19)
			{
				if (wzq[x][y]==0)
				{
					m_point.x=x;
					m_point.y=y;
				}
			}
		}
	return m_point;
}
