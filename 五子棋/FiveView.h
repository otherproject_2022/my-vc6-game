// FiveView.h : interface of the CFiveView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FIVEVIEW_H__FF7E7035_5220_49A2_8C7F_4F6CD8509BF0__INCLUDED_)
#define AFX_FIVEVIEW_H__FF7E7035_5220_49A2_8C7F_4F6CD8509BF0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFiveView : public CView
{
protected: // create from serialization only
	CFiveView();
	DECLARE_DYNCREATE(CFiveView)

// Attributes
public:
	CFiveDoc* GetDocument();

	HCURSOR hcursorwhite;
	HCURSOR hcursorblack;
	int wzq[19][19];
	bool colorwhite;
	CBitmap m_bmpblack;
	CBitmap m_bmpwhite;
	int vscup;
	int pleyercolor;
	int comcolor;
	bool computdown;
	CPoint playerlastpoint;
	CPoint comlastpoint;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFiveView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	CPoint SearchNear();
	int checkleave(int sum[]);
	int whiteputdown(int x,int y);
	int blackputdown(int x,int y);
	int comdefdown(CPoint &m_point);
	int comattputdown(CPoint&m_point);
	void cpudown();
	void putdown(int x,int y);
	void over(int x,int y);
	void Save();
	virtual ~CFiveView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFiveView)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnOpen();
	afx_msg void OnSave();
	afx_msg void OnStart();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnVscpuf();
	afx_msg void OnVscpus();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in FiveView.cpp
inline CFiveDoc* CFiveView::GetDocument()
   { return (CFiveDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIVEVIEW_H__FF7E7035_5220_49A2_8C7F_4F6CD8509BF0__INCLUDED_)
