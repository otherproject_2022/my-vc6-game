// Russia.h: interface for the CRussia class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RUSSIA_H__ADB3F3E0_EDBF_404E_A134_131D056713BE__INCLUDED_)
#define AFX_RUSSIA_H__ADB3F3E0_EDBF_404E_A134_131D056713BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRussia  
{
public:
	void DeleteLine();
	BOOL DeleteLines();
	void MoveDown();
	void Xiuzheng(int a[][4]);
	void Change();
	void MoveRight();
	void MoveLeft();
	CRussia();
	virtual ~CRussia();
	int Russia[100][100];
	int Now[4][4];
	int Will[4][4];
	int After[4][4];
	CPoint NowPosition;
	int Count;
	bool end;
	int m_Level;
	int m_Speed;
	int m_Score;
//	int m_RowCount,m_ColCount;
	CBitmap jiemian;
	CBitmap fangkuai;
	CBitmap BmpColor[7];

	int RussiaColor[100][100];
	int NowColor;
	int WillColor;

	void DrawScore(CDC*pDC);
	void LineDelete();
	void Move(int direction);
	bool Change(int a[][4],CPoint p,int b[][100]);
	bool Meet(int a[][4],int direction,CPoint p);
	void DrawWill();
	void DrawJiemian(CDC*pDC);
	void Start();
	int Max_x;
	int Max_y;
};

#endif // !defined(AFX_RUSSIA_H__ADB3F3E0_EDBF_404E_A134_131D056713BE__INCLUDED_)
