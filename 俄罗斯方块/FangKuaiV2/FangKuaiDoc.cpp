// FangKuaiDoc.cpp : implementation of the CFangKuaiDoc class
//

#include "stdafx.h"
#include "FangKuai.h"

#include "FangKuaiDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiDoc

IMPLEMENT_DYNCREATE(CFangKuaiDoc, CDocument)

BEGIN_MESSAGE_MAP(CFangKuaiDoc, CDocument)
	//{{AFX_MSG_MAP(CFangKuaiDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiDoc construction/destruction

CFangKuaiDoc::CFangKuaiDoc()
{
	// TODO: add one-time construction code here

}

CFangKuaiDoc::~CFangKuaiDoc()
{
}

BOOL CFangKuaiDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CFangKuaiDoc serialization

void CFangKuaiDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiDoc diagnostics

#ifdef _DEBUG
void CFangKuaiDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFangKuaiDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiDoc commands
