// FangKuai.h : main header file for the FANGKUAI application
//

#if !defined(AFX_FANGKUAI_H__9DC923D7_E7F3_4EB3_A1EE_37906E7C2FD2__INCLUDED_)
#define AFX_FANGKUAI_H__9DC923D7_E7F3_4EB3_A1EE_37906E7C2FD2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiApp:
// See FangKuai.cpp for the implementation of this class
//

class CFangKuaiApp : public CWinApp
{
public:
	CFangKuaiApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFangKuaiApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CFangKuaiApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FANGKUAI_H__9DC923D7_E7F3_4EB3_A1EE_37906E7C2FD2__INCLUDED_)
