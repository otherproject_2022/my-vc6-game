// FangKuaiView.cpp : implementation of the CFangKuaiView class
//

#include "stdafx.h"
#include "FangKuai.h"

#include "FangKuaiDoc.h"
#include "FangKuaiView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiView

IMPLEMENT_DYNCREATE(CFangKuaiView, CView)

BEGIN_MESSAGE_MAP(CFangKuaiView, CView)
	//{{AFX_MSG_MAP(CFangKuaiView)
	ON_COMMAND(ID_MENU_START, OnMenuStart)
	ON_WM_TIMER()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiView construction/destruction

CFangKuaiView::CFangKuaiView()
{
	// TODO: add construction code here
	fenmian.LoadBitmap(IDB_BITMAP1);
	start=false;
	m_bPause=false;

}

CFangKuaiView::~CFangKuaiView()
{
}

BOOL CFangKuaiView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiView drawing

void CFangKuaiView::OnDraw(CDC* pDC)
{
	CFangKuaiDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	CDC Dc;
	Dc.CreateCompatibleDC(pDC);
	if (!start)
	{
		Dc.SelectObject(fenmian);
		pDC->BitBlt(0,0,500,550,&Dc,0,0,SRCCOPY);
		Dc.DeleteDC();
	}
	else
		russia.DrawJiemian(pDC);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiView printing

BOOL CFangKuaiView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFangKuaiView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFangKuaiView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiView diagnostics

#ifdef _DEBUG
void CFangKuaiView::AssertValid() const
{
	CView::AssertValid();
}

void CFangKuaiView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFangKuaiDoc* CFangKuaiView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFangKuaiDoc)));
	return (CFangKuaiDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFangKuaiView message handlers

void CFangKuaiView::OnMenuStart() 
{
	// TODO: Add your command handler code here
	start=true;
	russia.Start();
	SetTimer(1,100*(11-russia.m_Speed),NULL);
}

void CFangKuaiView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if (russia.end)
	{
		KillTimer(1);
		return;
	}
	russia.MoveDown();

//	russia.NowPosition.y++;
//	russia.Move(3);
	russia.DrawJiemian(GetDC());
	CView::OnTimer(nIDEvent);
}

void CFangKuaiView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if (!start)
	{
		return;
	}
	if (m_bPause==TRUE)
	{
		return;
	}
	switch(nChar)
	{
	case VK_LEFT:
		russia.MoveLeft();
		break;
	case VK_RIGHT:
		russia.MoveRight();
		break;
	case VK_UP:
		russia.Change();
		break;
	case VK_DOWN:
		russia.MoveDown();
	//	russia.Move(3);
		break;
	}
	CDC*pDC=GetDC();
	russia.DrawJiemian(pDC);
	ReleaseDC(pDC);
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}
