// Russia.cpp: implementation of the CRussia class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FangKuai.h"
#include "Russia.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRussia::CRussia()
{
	jiemian.LoadBitmap(IDB_BITMAP2);
	fangkuai.LoadBitmap(IDB_BITMAP4);
}

CRussia::~CRussia()
{

}
void CRussia::DrawScore(CDC*pDC)
{
	int nOldDC=pDC->SaveDC();
	CFont font;
	font.CreatePointFont(300,"Comic Sans MS");
	pDC->SelectObject(font);
	CString str;
	pDC->SetTextColor(RGB(39,255,19));
	pDC->SetBkColor(RGB(255,255,0));
	str.Format("%d",m_Level);
	pDC->TextOut(440,120,str);
	str.Format("%d",m_Speed);
	pDC->TextOut(440,64,str);
	str.Format("%d",m_Score);
	pDC->TextOut(440,2,str);
	pDC->RestoreDC(nOldDC);
}
void CRussia::LineDelete()
{
	int m=0;
	bool flag=0;
	int i,j,k,l;
	for (j=0;j<Max_y;i++)
	{
		flag=true;
		for (i=0;i<Max_x;i++)
			if (Russia[i][j]==0)
				flag=false;
		if (flag==true)
		{
			m++;
			for (k=i;k>0;k--)
			{
			//	for(l=0;l<m_ColCount;l++)
//					Russia[k][l]=Russia[k-1][l];
			}
		//	for (l=0;l<m_ColCount;l++)
//				Russia[0][l]=0;
		}
	}
	DrawWill();
	if (m==0)
	{
		return;
	}
	switch(m)
	{
	case 1:
		m_Score++;
		break;
	case 2:
		m_Score+=3;
		break;
	case 3:
		m_Score+=6;
		break;
	case 4:
		m_Score+=10;
		break;
	}
	m_Speed=m_Score/100;
	for (i=0;i<4;i++)
	{
		for (j=0;j<4;j++)
		{
			if (Now[i][j]==1)
			{
				if (Russia[i+NowPosition.x][j+NowPosition.y]==1)
				{
					end=true;
					AfxMessageBox("游戏结束!");
					return;
				}
			}
		}
	}
}
void CRussia::Move(int direction)
{
	if (end)
	{
		return;
	}
	switch(direction)
	{
	case 1:
		if (Meet(Now,1,NowPosition))break;
		NowPosition.y--;
		break;
	case 2:
		if (Meet(Now,2,NowPosition))break;
		NowPosition.y++;
		break;
	case 3:
		if (Meet(Now,3,NowPosition))
		{
			LineDelete();
			break;
		}
		NowPosition.x++;
		break;
	case 4:
		if (Meet(Now,4,NowPosition))break;
		break;
	default:
		break;
	}

}
bool CRussia::Change(int a[][4],CPoint p,int b[][100])
{
	int tmp[4][4];
	int i,j;
	int k=4,l=4;
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
	{
			tmp[i][j]=a[j][3-i];
			After[i][j]=0;
	}
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
			if (tmp[i][j]==1)
			{
				if(k>i)k=i;
				if(l>j)l=j;
			}
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
		if (tmp[i][j]==1)
		{
			After[i][j]=tmp[i][j];
		}
		for (i=0;i<4;i++)
			for(j=0;j<4;j++)
		{
				if(After[i][j]==0)continue;
//				if(((p.x+i)>=m_RowCount)||((p.y+j)<0)||((p.y+j)>=m_ColCount))
					return false;
				if (b[p.x+i][p.y+j]==1)
				return false;
		}
	
	return TRUE;
}
bool CRussia::Meet(int a[][4],int direction,CPoint p)
{
	int i,j;
	int x,y;
		x=p.x;
		y=p.y;
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
			if(a[i][j]==1)
				Russia[p.x+i][p.y+j]=0;
	for(i=0;i<4;i++)
		for(j=0;j<4;j++)
		if(a[i][j]==1)
		{
			switch(direction)
			{
			case 1:
				if (p.y+j-1<0)goto exit;
				if (Russia[p.x+i][p.y+j-1]==1)goto exit;
				break;
			case 2:
//				if (p.y+j+1>=m_ColCount)goto exit;
				if (Russia[p.x+i][p.y+j+1]==1)goto exit;
				break;
			case 3:
			//	if (p.x+i+1>=m_RowCount-1)goto exit;
				if (Russia[p.x+i+1][p.y+j]==1)goto exit;
				break;
			case 4:
				if(!Change(a,p,Russia))goto exit;
				for (i=0;i<4;i++)
					for(j=0;j<4;j++)
					{
						Now[i][j]=After[i][j];
						a[i][j]=Now[i][j];
					}
				break;
			}
		}
	
		x=p.x;
		y=p.y;
		switch(direction)
		{
		case 1:
			y--;break;
		case 2:
			y++;break;
		case 3:
			x++;break;
		case 4:
			break;
		}
		for(i=0;i<4;i++)
		for (j=0;j<4;j++)
			if(a[i][j]==1)
				Russia[x+i][y+j]=1;
			return false;
exit:
// 		x=p.x;
// 		y=p.y;
			for(i=0;i<4;i++)
				for (j=0;j<4;j++)
					if(a[i][j]==1)
				Russia[x+i][y+j]=1;
			return TRUE;
}
void CRussia::DrawWill()
{
	int i,j;
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
	{
			Now[i][j]=Will[i][j];
			Will[i][j]=0;
	}

	int nTemp=rand()%Count;
	switch(nTemp)
	{
	case 0:
		Will[0][0]=1;
		Will[0][1]=1;
		Will[1][0]=1;
		Will[1][1]=1;
		break;
	case 1:
		Will[0][0]=1;
		Will[0][1]=1;
		Will[1][0]=1;
		Will[2][0]=1;
		break;
	case 2:
		Will[0][0]=1;
		Will[0][1]=1;
		Will[1][1]=1;
		Will[2][1]=1;
		break;
	case 3:
		Will[0][1]=1;
		Will[1][0]=1;
		Will[1][1]=1;
		Will[2][0]=1;
		break;
	case 4:
		Will[0][0]=1;
		Will[1][0]=1;
		Will[1][1]=1;
		Will[2][1]=1;
		break;
	case 5:
		Will[0][0]=1;
		Will[1][0]=1;
		Will[1][1]=1;
		Will[2][0]=1;
		break;
	case 6:
		Will[0][0]=1;
		Will[1][0]=1;
		Will[2][0]=1;
		Will[3][0]=1;
		break;
	case 7:
		Will[0][0]=1;
		Will[1][0]=1;
		Will[1][1]=1;
		Will[1][2]=1;
		Will[0][2]=1;
		break;
	case 8:
		Will[0][0]=1;
		Will[1][0]=1;
		Will[2][0]=1;
		Will[1][1]=1;
		Will[1][2]=1;
		break;
	}
	NowPosition.x=Max_x/2;
	NowPosition.y=0;
}
void CRussia::DrawJiemian(CDC*pDC)
{
	CDC Dc;
	Dc.CreateCompatibleDC(pDC);
	Dc.SelectObject(jiemian);
	pDC->BitBlt(0,0,500,550,&Dc,0,0,SRCCOPY);
	DrawScore(pDC);
	Dc.SelectObject(fangkuai);
	int i,j;
	for (i=0;i<Max_x;i++)
		for (j=0;j<Max_y;j++)
	{
			if (Russia[i][j]==1)
			{
				pDC->BitBlt(i*30,j*30,30,30,&Dc,0,0,SRCCOPY);
			}
	}
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
	{
		if (Now[i][j]==1)
		{
			pDC->BitBlt((NowPosition.x+i)*30,(NowPosition.y+j)*30,30,30,&Dc,0,0,SRCCOPY);
		}
	}
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
	{
		if (Will[i][j]==1)
		{
			pDC->BitBlt(365+i*30,240+j*30,30,30,&Dc,0,0,SRCCOPY);
		}
	}
	Dc.DeleteDC();

}
void CRussia::Start()
{
	end=false;
	m_Score=0;
	m_Speed=0;
	m_Level=0;
//	m_RowCount=18;//行
//	m_ColCount=12;//列
	Max_x=12;
	Max_y=17;
	Count=7;
	int i,j;
	for (i=0;i<Max_x;i++)
		for(j=0;j<Max_y;j++)
	{
			Russia[i][j]=0;
	}
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
	{
			Now[i][j]=0;
			Will[i][j]=0;
	}
	srand(GetTickCount());
	DrawWill();
	DrawWill();
}

void CRussia::MoveLeft()
{
	int i,j;
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
	if (Now[i][j]==1)
		if (((NowPosition.x+i-1)<0)||(Russia[NowPosition.x+i-1][NowPosition.y+j]==1))
		return;
	NowPosition.x--;
}

void CRussia::MoveRight()
{
	int i,j;
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
		if (Now[i][j]==1)
			if(((NowPosition.x+i+1)>=Max_x)||(Russia[NowPosition.x+i+1][NowPosition.y+j]==1))
				return;
	NowPosition.x++;
}

void CRussia::Change()
{
	int i,j;
	int temp[4][4];
	for (i=0;i<4;i++)
	  for(j=0;j<4;j++)
	  {
		temp[i][j]=Now[j][3-i];
	  }
	Xiuzheng(temp);
   for (i=0;i<4;i++)
	  for(j=0;j<4;j++)
		  if(temp[i][j]==1)
		  if(((NowPosition.x+i)<0)||((NowPosition.x+i)>=Max_x)||((NowPosition.y+j)>=Max_y)||(Russia[NowPosition.x+i][NowPosition.y+j]==1))
			return;
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
		{
			Now[i][j]=temp[i][j];
	  }

}

void CRussia::Xiuzheng(int a[][4])
{
	int i,j;
	bool flag;
	int x=0;
	int y=0;
	flag=TRUE;
	while(flag)
	{
		for (i=0;i<4;i++)
		{
			if (a[i][0]==1)
			{
				flag=false;
				break;
			}
		}
		if (flag)
		{
		 for(j=0;j<3;j++)
			for (i=0;i<4;i++)
			{
				a[i][j]=a[i][j+1];
				a[i][j+1]=0;
			}
		}
	}
	flag=TRUE;
	while(flag)
	{
		for (j=0;j<4;j++)
		{
			if (a[0][j]==1)
			{
				flag=false;
				break;
			}
		}
		if (flag)
		{
			for(i=0;i<3;i++)
				for (j=0;j<4;j++)
				{
					a[i][j]=a[i+1][j];
					a[i+1][j]=0;
				}
		}
	}
}

void CRussia::MoveDown()
{
	int i,j;
	bool flag=TRUE;
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
		{
			if (Now[i][j]==1&&flag)
				if(((NowPosition.y+j+1)>=Max_y)||(Russia[NowPosition.x+i][NowPosition.y+j+1]==1))
				{
					DeleteLine();				
					flag=FALSE;
				}
		}
	if (flag)
	{

	NowPosition.y++;
	}

}

void CRussia::DeleteLine()
{
	int i,j;
	int sum,n;
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
		{
			if (Now[i][j]==1)
			{
				Russia[NowPosition.x+i][NowPosition.y+j]=1;
			}				
		}
	n=1;
	sum=0;
	while(DeleteLines())
	{
		sum+=n;
		n++;
	}
	m_Score+=sum;
	m_Speed=m_Score/100;
	m_Level=m_Speed;
	DrawWill();
	for (i=0;i<4;i++)
		for(j=0;j<4;j++)
		{
			if (Now[i][j]==1)
				if(Russia[NowPosition.x+i][NowPosition.y+j]==1)
				{
					end=true;
					AfxMessageBox("游戏结束!");
					return;
				}
		}

}

BOOL CRussia::DeleteLines()
{
	
	int i,j,n=-1;
	BOOL flag=TRUE;
	for (j=0;j<Max_y;j++)
	{
		flag=TRUE;
		for (i=0;i<Max_x;i++)
		{
			if (Russia[i][j]==0)
			{
				flag=FALSE;
			}
		}
		if(flag)
		{
			n=j;
			break;
		}
	}
	if (n==-1)
		return false;
	for (j=n;j>0;j--)
		for(i=0;i<Max_x;i++)
			Russia[i][j]=Russia[i][j-1];
	for(i=0;i<Max_x;i++)
			Russia[i][0]=0;
	return TRUE;
}
