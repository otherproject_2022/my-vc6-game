// FangKuaiView.h : interface of the CFangKuaiView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FANGKUAIVIEW_H__90282F10_18E3_4376_A524_0AC594FA7FED__INCLUDED_)
#define AFX_FANGKUAIVIEW_H__90282F10_18E3_4376_A524_0AC594FA7FED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//class CRussia;
#include "Russia.h"
class CFangKuaiView : public CView
{
protected: // create from serialization only
	CFangKuaiView();
	DECLARE_DYNCREATE(CFangKuaiView)

// Attributes
public:
	CFangKuaiDoc* GetDocument();

	CRussia russia;
	bool start;
	CBitmap fenmian;
	bool m_bPause;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFangKuaiView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFangKuaiView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFangKuaiView)
	afx_msg void OnMenuStart();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in FangKuaiView.cpp
inline CFangKuaiDoc* CFangKuaiView::GetDocument()
   { return (CFangKuaiDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FANGKUAIVIEW_H__90282F10_18E3_4376_A524_0AC594FA7FED__INCLUDED_)
