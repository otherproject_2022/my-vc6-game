// ComInfo.h: interface for the CComInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMINFO_H__EF4750EF_0D69_48E2_AAF4_F8F423569847__INCLUDED_)
#define AFX_COMINFO_H__EF4750EF_0D69_48E2_AAF4_F8F423569847__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define NOCARD	-1
#define ONECARD   1
#define TWOCARD   2
#define THREECARD	3
#define FOURCARD    4
#define TWOPAIR		5
#define THREEONE	6
#define THREETWO	7
#define WATER		8
#define WATERTWO	9
#define WATERTHREAD	10 


struct cardinfo
{
	HBITMAP hBitmap;
	int num;
	int color;
};

struct playercardinfo{
	int ID;
	bool Click;
	int x;
	int y;
};
 
struct zhoucardinfo{
	int nums;
	int type;
	int keynum;
};

class CComInfo  
{
public:
	CComInfo();
	virtual ~CComInfo();

};

#endif // !defined(AFX_COMINFO_H__EF4750EF_0D69_48E2_AAF4_F8F423569847__INCLUDED_)
