// Player.cpp: implementation of the Player class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyCard.h"
#include "Player.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Player::Player()
{
num=0;
selcard=NULL;
pcard=NULL;
}

Player::~Player()
{

}

void Player::Paixu()
{
	int i,j,temp;
	int tempid;
	for (i=0;i<num-1;i++)
	{
		temp=i;
		for (j=i+1;j<num;j++)
		{
			if (pcard[m_card[j].ID].num>pcard[m_card[temp].ID].num)
			{
				temp=j;
			}
		}
		if (temp!=i)
		{
			tempid=m_card[i].ID;
			m_card[i].ID=m_card[temp].ID;
			m_card[temp].ID=tempid;
		}
	}

}

BOOL Player::Chuapai()
{
	int i;
	selcard->num=0;
	int n=0;
	for(i=0;i<num;i++)
	{
		if (m_card[i].Click)
		{
		selcard->m_card[n++].ID=m_card[i].ID;	
		}
	}
	selcard->num=n;
	//	AfxMessageBox("1��Ƥ");
	selcard->CheckCardType();
//	AfxMessageBox("��Ƥ");
	if (selcard->info.type!=NOCARD)
	{
	for(i=0;i<num;i++)
	{
		if (m_card[i].Click)
		{
			m_card[i]=m_card[num-1];
			i--;
			num--;		
		}
	}
		ResetAll();
		return TRUE;
	}
	else
	{
		ResetAll();
		return FALSE;
	}

}

void Player::DrawCard(CDC *pDC)
{

	if (num<=0)
	{
		return;
	}
		Paixu();
	
	CDC pdc;
	pdc.CreateCompatibleDC(pDC);
	int i=0;
	HBITMAP m_bmp=(HBITMAP)pdc.SelectObject(pcard[m_card[0].ID].hBitmap);

	int star=x-30*(num/2+2);
	for (i=0;i<num;i++)
	{
		pdc.SelectObject(pcard[m_card[i].ID].hBitmap);
		if (m_card[i].Click)
		{
			pDC->BitBlt(star+30*i,y-30,71,96,&pdc,0,0,MERGECOPY);
		}
		else
		{
			pDC->BitBlt(star+30*i,y,71,96,&pdc,0,0,MERGECOPY);
		}
	}
	pdc.SelectObject(m_bmp);
}

BOOL Player::ChickChoice(CPoint point)
{
	int choice=-1;
	int i;
	int star=x-30*(num/2+2);
	int tempx,tempy;
	for (i=0;i<num;i++)
	{
		tempx=star+30*i;
		if (m_card[i].Click)
			tempy=y-30;
		else
			tempy=y;
		if ((point.x>tempx)&&(point.x<(tempx+71))&&(point.y>tempy)&&(point.y<(tempy+96)))
		{
			choice=i;
		}
	}
	if (choice!=-1)
	{
		m_card[choice].Click=!m_card[choice].Click;
		return TRUE;
	}
	return FALSE;
}

void Player::DrawCardOnLeft(CDC *pDC,HBITMAP hBmp)
{
	if (num<=0)
	{
		return;
	}
	CDC pdc;
	pdc.CreateCompatibleDC(pDC);
	int i=0;
	HBITMAP m_bmp=(HBITMAP)pdc.SelectObject(hBmp);
	
	int star=y-30*(num/2+2);
	for (i=0;i<num;i++)
	{
		
			pDC->BitBlt(x,star+i*30,71,96,&pdc,0,0,MERGECOPY);
	
	}
	pdc.SelectObject(m_bmp);
}

void Player::DrawCardOnUp(CDC *pDC, HBITMAP hBmp)
{
	if (num<=0)
	{
		return;
	}
	CDC pdc;
	pdc.CreateCompatibleDC(pDC);
	int i=0;
	HBITMAP m_bmp=(HBITMAP)pdc.SelectObject(hBmp);
	
	int star=x-30*(num/2+2);
	for (i=0;i<num;i++)
	{
		pDC->BitBlt(star+30*i,y,71,96,&pdc,0,0,MERGECOPY);
	}
	pdc.SelectObject(m_bmp);
}

void Player::ResetAll()
{
	int i;
	for (i=0;i<num;i++)
	{
		m_card[i].Click=false;
	}
}
Player Player::operator=(Player pleyerother)
{
	int i;
	num=pleyerother.num;
	for (i=0;i<num;i++)
	{
		m_card[i]=pleyerother.m_card[i];
	}
	return *this;
}

void Player::CheckCardType()
{
	info.type=NOCARD;
	info.nums=0;
	if (num<=0)
	{
		return;
	}
	Paixu();
	IsOneCard();
	IsTwoCard();
	IsThreeCard();
	IsFoueCard();
	IsThreeOne();
	IsTwoPair();

}

BOOL Player::IsOneCard()
{
	if (num!=1)
	{
		return FALSE;
	}
	info.type=ONECARD;
	info.nums=1;
	info.keynum=pcard[m_card[0].ID].num;
	return TRUE;
}

BOOL Player::IsTwoCard()
{
	if (num!=2)
	{
		return FALSE;
	}
	if(!CheckSome(0,2))
	{
		return FALSE;
	}
	info.type=TWOCARD;
	info.nums=2;
	info.keynum=pcard[m_card[0].ID].num;
	return TRUE;
}

BOOL Player::IsThreeCard()
{
	if (num!=3)
	{
		return FALSE;
	}
	if(!CheckSome(0,3))
	{
		return FALSE;
	}
	info.type=THREECARD;
	info.nums=3;
	info.keynum=pcard[m_card[0].ID].num;
	return TRUE;
}

BOOL Player::IsFoueCard()
{
	if (num!=4)
	{
		return FALSE;
	}
	if(!CheckSome(0,4))
	{
		return FALSE;
	}
	info.type=FOURCARD;
	info.nums=4;
	info.keynum=pcard[m_card[0].ID].num;
	return TRUE;
}

BOOL Player::IsTwoPair()
{
	if (num!=4)
	{
		return FALSE;
	}
	if(!(CheckSome(0,2)&&CheckSome(2,2)))
	{
		return FALSE;
	}
	if (ChechSomeNum(1,2)!=1)
	{
		return FALSE;
	}
	info.type=TWOPAIR;
	info.nums=4;
	info.keynum=pcard[m_card[0].ID].num;
	return TRUE;
}

BOOL Player::IsThreeOne()
{
	if (num!=4)
	{
		return FALSE;
	}
	if (CheckSome(0,3)&&ChechSomeNum(0,3)!=0)
	{
		info.type=THREEONE;
		info.nums=4;
		info.keynum=pcard[m_card[0].ID].num;
		return TRUE;
	}
	if (CheckSome(1,3)&&ChechSomeNum(0,3)!=0)
	{
		info.type=THREEONE;
		info.nums=4;
		info.keynum=pcard[m_card[1].ID].num;
		return TRUE;
	}
	return FALSE;
}

BOOL Player::CheckSome(int star, int nums)
{
	if ((star+nums)>num)
	{
		return FALSE;
	}
	BOOL	flag=TRUE;
	int firstnum=pcard[m_card[star].ID].num;
	int i;
	for(i=1;i<nums;i++)
	{
		if (firstnum!=pcard[m_card[i+star].ID].num)
		{
			flag=FALSE;
			break;
		}
	}
	return flag;
}

int Player::ChechSomeNum(int a, int b)
{
	if (a<b)
	{
		return	pcard[m_card[a].ID].num-pcard[m_card[b].ID].num;
	}
	else
	{
		return	pcard[m_card[b].ID].num-pcard[m_card[a].ID].num;
	}

}
