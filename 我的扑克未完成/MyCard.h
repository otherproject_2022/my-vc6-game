// MyCard.h : main header file for the MYCARD application
//

#if !defined(AFX_MYCARD_H__7762FDE6_10ED_49DD_848C_4663AE972784__INCLUDED_)
#define AFX_MYCARD_H__7762FDE6_10ED_49DD_848C_4663AE972784__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

   

/////////////////////////////////////////////////////////////////////////////
// CMyCardApp:
// See MyCard.cpp for the implementation of this class
//

class CMyCardApp : public CWinApp
{
public:
	CMyCardApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyCardApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	//{{AFX_MSG(CMyCardApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYCARD_H__7762FDE6_10ED_49DD_848C_4663AE972784__INCLUDED_)
