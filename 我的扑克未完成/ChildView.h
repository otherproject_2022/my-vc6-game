// ChildView.h : interface of the CChildView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHILDVIEW_H__B1DF6D84_F61B_4BD7_B25C_30A85E1BAAAA__INCLUDED_)
#define AFX_CHILDVIEW_H__B1DF6D84_F61B_4BD7_B25C_30A85E1BAAAA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
 
#include "Player.h"
#define MYBUTTON1 (WM_USER+1001)
#define MYBUTTONSTAR (WM_USER+1002)
//71*96
/////////////////////////////////////////////////////////////////////////////
// CChildView window

class CChildView : public CWnd
{
// Construction
public:
	CButton m_Button1;
	CButton m_ButtonStar;
	CChildView();
cardinfo m_mycard[52];
cardinfo m_myback;
Player user;
Player leftuser;
Player rightuser;
Player upuser;
Player showuse;
Player tempcard;
zhoucardinfo m_info;
HBITMAP m_BackBmp;
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChildView)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	void DrawBack(CDC*pDC);
	void ReadCard();
	void StarCard();
	void DrawCard(CDC*pDC);
	virtual ~CChildView();

	// Generated message map functions

protected:
	//{{AFX_MSG(CChildView)
		afx_msg void OnBtnDown();
		afx_msg void OnBtnStar();
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__B1DF6D84_F61B_4BD7_B25C_30A85E1BAAAA__INCLUDED_)
