// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"
#include "MyCard.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildView

CChildView::CChildView()
{
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView,CWnd )
	//{{AFX_MSG_MAP(CChildView)
	ON_BN_CLICKED(MYBUTTON1,OnBtnDown)
	ON_BN_CLICKED(MYBUTTONSTAR,OnBtnStar)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
//	cs.cx=1366;
//	cs.cy=1266;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), HBRUSH(COLOR_WINDOW+1), NULL);
	


	return TRUE;
}

void CChildView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	CDC temp;
	CRect myRect;
	CBitmap MemBitmap;
	GetClientRect(&myRect);
//	temp.CreateCompatibleDC(&dc);
	temp.CreateCompatibleDC(NULL);
	MemBitmap.CreateCompatibleBitmap(&dc,myRect.right,myRect.bottom);
	CBitmap*pOld=temp.SelectObject(&MemBitmap);
	temp.FillSolidRect(0,0,myRect.right,myRect.bottom,RGB(255,255,255));
	DrawBack(&temp);
	DrawCard(&temp);
	dc.BitBlt(0,0,myRect.right,myRect.bottom,&temp,0,0,SRCCOPY);
	MemBitmap.DeleteObject();
	temp.DeleteDC();
}


void CChildView::DrawCard(CDC *pDC)
{
	
	user.DrawCard(pDC);
	leftuser.DrawCardOnLeft(pDC,m_myback.hBitmap);
	rightuser.DrawCardOnLeft(pDC,m_myback.hBitmap);

	upuser.DrawCardOnUp(pDC,m_myback.hBitmap);

	showuse.DrawCard(pDC);
	
}

void CChildView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(user.ChickChoice(point))
	{
		Invalidate();
	}


	CWnd ::OnLButtonDown(nFlags, point);
}


void CChildView::OnSize(UINT nType, int cx, int cy) 
{
	CWnd ::OnSize(nType, cx, cy);
	
	int x,y;
	user.y=cy-150;
	user.x=cx/2+1;
	leftuser.x=50;
	leftuser.y=cy/2+1;
	rightuser.x=cx-150;
	rightuser.y=cy/2+1;
	upuser.x=cx/2+1;
	upuser.y=50;
	showuse.x=cx/2+1;
	showuse.y=cy/2-35;
	y=cy-230;
	x=cx/2+30*4;
	m_Button1.MoveWindow(x,y,70,30);
	m_ButtonStar.MoveWindow(x-100,y,70,30);

}
void CChildView::OnBtnDown()
{
	if(	user.Chuapai())
	{	
		showuse=tempcard;
		m_info=tempcard.info;
	}
	else
	{
		AfxMessageBox("出牌不和规则");
	}
		Invalidate();
}
void CChildView::OnBtnStar()
{
	StarCard();
	Invalidate();
}
int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd ::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	ReadCard();
	RECT myRect;
	myRect.left=100;
	myRect.right=150;
	myRect.top=200;
	myRect.bottom=250;
	m_Button1.Create("出牌",WS_CHILD|WS_VISIBLE|WS_BORDER,myRect,this,MYBUTTON1);
	m_Button1.ShowWindow(SW_SHOW);
	m_ButtonStar.Create("重新开始",WS_CHILD|WS_VISIBLE|WS_BORDER,myRect,this,MYBUTTONSTAR);
	m_ButtonStar.ShowWindow(SW_SHOW);
	StarCard();
	return 0;
}

void CChildView::StarCard()
{
	srand(time(NULL));
	int num[52];
	int i;
	int temp;
	int tempnum;
	for(i=0;i<52;i++)
		num[i]=i;
	for (i=52;i>0;i--)
	{
		Sleep(5);
		temp=rand()%i;
		tempnum=num[i-1];
		num[i-1]=num[temp];
		num[temp]=tempnum;
	}

	for (i=0;i<16;i++)
	{
		user.m_card[i].ID=num[i];
		user.m_card[i].Click=false;	
	}
	user.num=16;
	for (i=0;i<16;i++)
	{
		leftuser.m_card[i].ID=num[i+16];
		leftuser.m_card[i].Click=false;	
	}
	leftuser.num=16;
	for (i=0;i<16;i++)
	{
		rightuser.m_card[i].ID=num[i+32];
		rightuser.m_card[i].Click=false;	
	}
	rightuser.num=16;
	upuser.num=0;
	showuse.num=0;
	
	return;


	for (i=0;i<13;i++)
	{
		user.m_card[i].ID=num[i];
		user.m_card[i].Click=false;	
	}
	user.num=13;
	for (i=0;i<13;i++)
	{
		leftuser.m_card[i].ID=num[i+13];
		leftuser.m_card[i].Click=false;	
	}
	leftuser.num=13;
	for (i=0;i<13;i++)
	{
		rightuser.m_card[i].ID=num[i+26];
		rightuser.m_card[i].Click=false;	
	}
	rightuser.num=13;
	for (i=0;i<13;i++)
	{
		upuser.m_card[i].ID=num[i+39];
		upuser.m_card[i].Click=false;	
	}
	upuser.num=13;

}

void CChildView::ReadCard()
{
	int i=0;
	int temp;
	int star=IDB_BITMAP01;
	for (i=0;i<52;i++)
	{
		m_mycard[i].hBitmap=LoadBitmap(::AfxGetInstanceHandle(),MAKEINTRESOURCE(star++));
		temp=i%13+1;
		if (temp<3)
		{
			temp+=13;
		}
		m_mycard[i].num=temp;
		m_mycard[i].color=i/13;
		
	}
	m_myback.hBitmap=LoadBitmap(::AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP100));
	m_BackBmp=LoadBitmap(::AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAPBACK));
	user.pcard=m_mycard;
	leftuser.pcard=m_mycard;
	rightuser.pcard=m_mycard;
	upuser.pcard=m_mycard;
	showuse.pcard=m_mycard;
	tempcard.pcard=m_mycard;

	user.selcard=&tempcard;
	leftuser.selcard=&tempcard;
	rightuser.selcard=&tempcard;
	upuser.selcard=&tempcard;

	tempcard.oldinfo=&m_info;

}

void CChildView::DrawBack(CDC *pDC)
{
	CDC Dc;
	Dc.CreateCompatibleDC(pDC);
	HBITMAP m_old=(HBITMAP)Dc.SelectObject(m_BackBmp);
	RECT myRect;
	GetClientRect(&myRect);
	pDC->StretchBlt(0,0,myRect.right,myRect.bottom,&Dc,0,0,800,600,SRCCOPY);
	Dc.SelectObject(m_old);
}

BOOL CChildView::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	return TRUE;
	return CWnd ::OnEraseBkgnd(pDC);
}
