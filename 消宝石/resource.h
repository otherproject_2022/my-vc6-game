//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BaoshiGame.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_BAOSHITYPE                  129
#define IDB_BITMAP_RED                  130
#define IDB_BITMAP_BLUE                 131
#define IDB_BITMAP_GREEN                132
#define IDB_BITMAP_BLUELESS             133
#define IDB_BITMAP_YELLOW               134
#define IDB_BITMAP_ORANGE               135
#define IDB_BITMAP_PURPLE               136
#define IDB_BITMAP_COLORFUL             137
#define ID_START                        32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
