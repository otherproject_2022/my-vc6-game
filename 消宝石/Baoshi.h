// Baoshi.h: interface for the CBaoshi class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BAOSHI_H__52D78CBA_D7DC_496B_98DE_4661E546DBA2__INCLUDED_)
#define AFX_BAOSHI_H__52D78CBA_D7DC_496B_98DE_4661E546DBA2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define BAOHSI_SIZE   100

#define BAOSHI_COLORFUL 0x0
#define BAOSHI_RED 0x1
#define BAOSHI_BLUE 0x2
#define BAOSHI_GREEN 0x3
#define BAOSHI_YELLOW 0x4
#define BAOSHI_ORANGE 0x5
#define BAOSHI_PURPLE 0x6
#define BAOSHI_BLUELESS 0x7


struct BaoshiInfo
{
	int color;
	bool willdelete;
};

class CBaoshi  
{
public:
	void Start();
	BOOL Check(CPoint point);
	BOOL Check();
	BOOL Delete();
	void change();
	void ClickPoint(CPoint point);
	void SetSize(CPoint point,INT r);
	BITMAP m_picsize; 
	DWORD m_size;
	CPoint m_start;
	void LoadPic();
	CBitmap m_test;
	CBitmap m_pic[8];
	void Chushihua();
	BaoshiInfo Baoshi[8][8];
	void DrawBaoshi(CDC*pDC);
	CBaoshi();
	virtual ~CBaoshi();
	CPoint m_old,m_new;

};

#endif // !defined(AFX_BAOSHI_H__52D78CBA_D7DC_496B_98DE_4661E546DBA2__INCLUDED_)
