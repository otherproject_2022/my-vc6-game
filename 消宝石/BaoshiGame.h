// BaoshiGame.h : main header file for the BAOSHIGAME application
//

#if !defined(AFX_BAOSHIGAME_H__87F826F1_0C2E_4294_9E55_D6B32385E278__INCLUDED_)
#define AFX_BAOSHIGAME_H__87F826F1_0C2E_4294_9E55_D6B32385E278__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameApp:
// See BaoshiGame.cpp for the implementation of this class
//

class CBaoshiGameApp : public CWinApp
{
public:
	CBaoshiGameApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaoshiGameApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CBaoshiGameApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BAOSHIGAME_H__87F826F1_0C2E_4294_9E55_D6B32385E278__INCLUDED_)
