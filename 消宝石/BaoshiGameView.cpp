// BaoshiGameView.cpp : implementation of the CBaoshiGameView class
//

#include "stdafx.h"
#include "BaoshiGame.h"

#include "BaoshiGameDoc.h"
#include "BaoshiGameView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameView

IMPLEMENT_DYNCREATE(CBaoshiGameView, CView)

BEGIN_MESSAGE_MAP(CBaoshiGameView, CView)
	//{{AFX_MSG_MAP(CBaoshiGameView)
	ON_COMMAND(ID_START, OnStart)
	ON_WM_KEYDOWN()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameView construction/destruction

CBaoshiGameView::CBaoshiGameView()
{
	// TODO: add construction code here

}

CBaoshiGameView::~CBaoshiGameView()
{
}

BOOL CBaoshiGameView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameView drawing

void CBaoshiGameView::OnDraw(CDC* pDC)
{
	CBaoshiGameDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	SetSize();
	m_Baoshi.DrawBaoshi(pDC);
//	pDC->TextOut(0,0,"�Ͱ�");
	// TODO: add draw code for native data here
	CDC Dc;
	Dc.CreateCompatibleDC(pDC);

	//	Chushihua();
	//	LoadPic();
	
// 	int i,j;
// 	for (i=0;i<8;i++)
// 		for(j=0;j<8;j++)
// 		{
// 		//	Dc.SelectObject(&m_test);
// 			pDC->BitBlt(i*30,j*30,30,30,&Dc,30,30,SRCCOPY);
// 			//	pDC->TextOut(i*30,j*30,"Hello");
// 	}

}

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameView printing

BOOL CBaoshiGameView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CBaoshiGameView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CBaoshiGameView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameView diagnostics

#ifdef _DEBUG
void CBaoshiGameView::AssertValid() const
{
	CView::AssertValid();
}

void CBaoshiGameView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CBaoshiGameDoc* CBaoshiGameView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CBaoshiGameDoc)));
	return (CBaoshiGameDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameView message handlers

void CBaoshiGameView::OnStart() 
{
	// TODO: Add your command handler code here
	CDC *Dc=GetDC();
	m_Baoshi.DrawBaoshi(Dc);
	
}

void CBaoshiGameView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CBaoshiGameView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CPoint m_Input;
	m_Input.x=(point.x-m_start.x)/m_size;
	m_Input.y=(point.y-m_start.y)/m_size;
	if (m_Input.x<0||m_Input.y<0||m_Input.x>=8||m_Input.y>=8)
	{
		return;
	}
	m_Baoshi.ClickPoint(m_Input);
	Invalidate();
	CView::OnLButtonDown(nFlags, point);
}

void CBaoshiGameView::SetSize()
{
	CRect m_rect;
	GetClientRect(&m_rect);
	int w=m_rect.right-m_rect.left;
	int h=m_rect.bottom-m_rect.top;
	int r=(w/10)<(h/10)?(w/10):(h/10);
	m_size=r;
	m_start.x=w/2-r*4;
	m_start.y=h/2-r*4;
	m_Baoshi.SetSize(m_start,m_size);
}
