// BaoshiGameView.h : interface of the CBaoshiGameView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BAOSHIGAMEVIEW_H__6EDCBC03_2C61_48CA_8C5E_5AF3D227F0A3__INCLUDED_)
#define AFX_BAOSHIGAMEVIEW_H__6EDCBC03_2C61_48CA_8C5E_5AF3D227F0A3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Baoshi.h"
class CBaoshiGameView : public CView
{
protected: // create from serialization only
	CBaoshiGameView();
	DECLARE_DYNCREATE(CBaoshiGameView)

// Attributes
public:
	CBaoshiGameDoc* GetDocument();
	CBaoshi m_Baoshi;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaoshiGameView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetSize();
	int m_size;
	CPoint m_start;
	virtual ~CBaoshiGameView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBaoshiGameView)
	afx_msg void OnStart();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in BaoshiGameView.cpp
inline CBaoshiGameDoc* CBaoshiGameView::GetDocument()
   { return (CBaoshiGameDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BAOSHIGAMEVIEW_H__6EDCBC03_2C61_48CA_8C5E_5AF3D227F0A3__INCLUDED_)
