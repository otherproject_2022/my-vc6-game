// BaoshiGameDoc.cpp : implementation of the CBaoshiGameDoc class
//

#include "stdafx.h"
#include "BaoshiGame.h"

#include "BaoshiGameDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameDoc

IMPLEMENT_DYNCREATE(CBaoshiGameDoc, CDocument)

BEGIN_MESSAGE_MAP(CBaoshiGameDoc, CDocument)
	//{{AFX_MSG_MAP(CBaoshiGameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameDoc construction/destruction

CBaoshiGameDoc::CBaoshiGameDoc()
{
	// TODO: add one-time construction code here

}

CBaoshiGameDoc::~CBaoshiGameDoc()
{
}

BOOL CBaoshiGameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameDoc serialization

void CBaoshiGameDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameDoc diagnostics

#ifdef _DEBUG
void CBaoshiGameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CBaoshiGameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBaoshiGameDoc commands
