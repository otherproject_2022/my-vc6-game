// Baoshi.cpp: implementation of the CBaoshi class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BaoshiGame.h"
#include "Baoshi.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBaoshi::CBaoshi()
{
	
	LoadPic();
	Chushihua();
	Start();
}

CBaoshi::~CBaoshi()
{

}

void CBaoshi::DrawBaoshi(CDC *pDC)
{
	CDC Dc;
	if(Dc.CreateCompatibleDC(pDC)==FALSE)
	{
		AfxMessageBox("����ʧ��");
	}
//	Chushihua();
//	LoadPic();
	pDC->MoveTo(m_start.x-1,m_start.y-1);
	pDC->LineTo(m_start.x+1+m_size*8,m_start.y-1);
	pDC->LineTo(m_start.x+1+m_size*8,m_start.y+1+m_size*8);
	pDC->LineTo(m_start.x-1,m_start.y+1+m_size*8);
	pDC->LineTo(m_start.x-1,m_start.y-1);
	int i,j;
	for (i=0;i<8;i++)
		for(j=0;j<8;j++)
	{
		Dc.SelectObject(&m_pic[Baoshi[i][j].color]);
		pDC->StretchBlt(m_start.x+m_size*i,m_start.y+m_size*j,m_size,m_size, &Dc,0, 0, m_picsize.bmWidth,m_picsize.bmHeight, SRCCOPY);

	}
	if (m_old.x!=-1)
	{
		pDC->MoveTo(m_start.x+m_old.x*m_size,m_start.y+m_old.y*m_size);
		pDC->LineTo(m_start.x+(m_old.x+1)*m_size,m_start.y+m_old.y*m_size);
		pDC->LineTo(m_start.x+(m_old.x+1)*m_size,m_start.y+(m_old.y+1)*m_size);
		pDC->LineTo(m_start.x+m_old.x*m_size,m_start.y+(m_old.y+1)*m_size);
		pDC->LineTo(m_start.x+m_old.x*m_size,m_start.y+m_old.y*m_size);

	}

}

void CBaoshi::Chushihua()
{

		m_start.x=200;
		m_start.y=20;
		m_size=40;
	
}

void CBaoshi::LoadPic()
{
	m_pic[BAOSHI_COLORFUL].LoadBitmap(IDB_BITMAP_COLORFUL);
	m_pic[BAOSHI_RED].LoadBitmap(IDB_BITMAP_RED);
	m_pic[BAOSHI_YELLOW].LoadBitmap(IDB_BITMAP_YELLOW);
	m_pic[BAOSHI_PURPLE].LoadBitmap(IDB_BITMAP_PURPLE);
	m_pic[BAOSHI_BLUELESS].LoadBitmap(IDB_BITMAP_BLUELESS);
	m_pic[BAOSHI_BLUE].LoadBitmap(IDB_BITMAP_BLUE);
	m_pic[BAOSHI_ORANGE].LoadBitmap(IDB_BITMAP_ORANGE);
	m_pic[BAOSHI_GREEN].LoadBitmap(IDB_BITMAP_GREEN);

	m_pic[0].GetBitmap(&m_picsize);

	m_test.LoadBitmap(IDB_BITMAP_GREEN);
}

void CBaoshi::SetSize(CPoint point, INT r)
{
	m_start=point;
	m_size=r;
}

void CBaoshi::ClickPoint(CPoint point)
{

	if (m_old.x==-1)
	{
		m_old=point;
		return;
	}
	int x=m_old.x-point.x;
	int y=m_old.y-point.y;
	int s=x*x+y*y;
	if (s==0)
	{
		m_old.x=-1;
		return;
	}
	if (s>1)
	{
		m_old=point;
		return;
	}
	m_new=point;
	change();

}

void CBaoshi::change()
{
	int n=Baoshi[m_old.x][m_old.y].color;
	Baoshi[m_old.x][m_old.y].color=Baoshi[m_new.x][m_new.y].color;
	Baoshi[m_new.x][m_new.y].color=n;
	if (Check())
	{
		m_old.x=-1;
			Delete();
 			while(Check())
 				Delete();
}
	else
	{
		Baoshi[m_new.x][m_new.y].color=Baoshi[m_old.x][m_old.y].color;
		Baoshi[m_old.x][m_old.y].color=n;
		m_old=m_new;
	}

}

BOOL CBaoshi::Delete()
{
	int i,j;
	int a,b,c,n=0;
	bool flag;
	for (i=0;i<8;i++)
	{
		flag=false;
		n=0;
		for (j=7;j>=0;j--)
		{
			if (Baoshi[i][j].willdelete)
			{
				a=j;
				n++;
				flag=true;
			}
		}
		if (flag)
		{
			b=a+n-1;
			while((b-n)>=0)
			{
				Baoshi[i][b]=Baoshi[i][b-n];
			
				b--;
			}
			for(c=0;c<n;c++)
			{	Baoshi[i][c].color=rand()%8;
				Baoshi[i][c].willdelete=false;
			}
		
		}
	}
	return TRUE;	
}

BOOL CBaoshi::Check()
{
	bool flag=false;
	int i,j;
	CPoint p;
	for (i=0;i<8;i++)
		for(j=0;j<8;j++)
	{
			p.x=i;
			p.y=j;
			if (Check(p))
			{
				flag=true;
			}
	}

	return flag;
}

BOOL CBaoshi::Check(CPoint point)
{

	bool flag=false;
	CPoint p1,p2,p;
	int n1,n2,n;
	p1=p2=p=point;
	p1.x-=2;
	p2.x-=1;
	n=Baoshi[p.x][p.y].color;
	n1=Baoshi[p1.x][p1.y].color;
	n2=Baoshi[p2.x][p2.y].color;

	if (p1.x>=0)
	{
		if((n==n1&&n==n2&&n1==n2)||((n==n1||n==n2||n1==n2)&&(n==BAOSHI_COLORFUL||n1==BAOSHI_COLORFUL||n2==BAOSHI_COLORFUL)))
		{
			flag=true;
			Baoshi[p.x][p.y].willdelete=true;
			Baoshi[p1.x][p1.y].willdelete=true;
			Baoshi[p2.x][p2.y].willdelete=true;
		}
	}
/////////////////////////////////////
	p1=p2=p=point;
	p1.x+=2;
	p2.x+=1;
	n=Baoshi[p.x][p.y].color;
	n1=Baoshi[p1.x][p1.y].color;
	n2=Baoshi[p2.x][p2.y].color;
	
	if (p1.x<8)
	{
		if((n==n1&&n==n2&&n1==n2)||((n==n1||n==n2||n1==n2)&&(n==BAOSHI_COLORFUL||n1==BAOSHI_COLORFUL||n2==BAOSHI_COLORFUL)))
		{
			flag=true;
			Baoshi[p.x][p.y].willdelete=true;
			Baoshi[p1.x][p1.y].willdelete=true;
			Baoshi[p2.x][p2.y].willdelete=true;
		}
	}
///////////////////////////////
	p1=p2=p=point;
	p1.y-=2;
	p2.y-=1;
	n=Baoshi[p.x][p.y].color;
	n1=Baoshi[p1.x][p1.y].color;
	n2=Baoshi[p2.x][p2.y].color;
	
	if (p1.y>=0)
	{
		if((n==n1&&n==n2&&n1==n2)||((n==n1||n==n2||n1==n2)&&(n==BAOSHI_COLORFUL||n1==BAOSHI_COLORFUL||n2==BAOSHI_COLORFUL)))
		{
			flag=true;
			Baoshi[p.x][p.y].willdelete=true;
			Baoshi[p1.x][p1.y].willdelete=true;
			Baoshi[p2.x][p2.y].willdelete=true;
		}
	}
/////////////////////////////////////////
	p1=p2=p=point;
	p1.y+=2;
	p2.y+=1;
	n=Baoshi[p.x][p.y].color;
	n1=Baoshi[p1.x][p1.y].color;
	n2=Baoshi[p2.x][p2.y].color;
	
	if (p1.y<8)
	{
		if((n==n1&&n==n2&&n1==n2)||((n==n1||n==n2||n1==n2)&&(n==BAOSHI_COLORFUL||n1==BAOSHI_COLORFUL||n2==BAOSHI_COLORFUL)))
		{
			flag=true;
			Baoshi[p.x][p.y].willdelete=true;
			Baoshi[p1.x][p1.y].willdelete=true;
			Baoshi[p2.x][p2.y].willdelete=true;
		}
	}
////////////////////////////////////
	p1=p2=p=point;
	p1.y+=1;
	p2.y-=1;
	n=Baoshi[p.x][p.y].color;
	n1=Baoshi[p1.x][p1.y].color;
	n2=Baoshi[p2.x][p2.y].color;
	
	if (p1.y<8&&p2.y>=0)
	{
		if((n==n1&&n==n2&&n1==n2)||((n==n1||n==n2||n1==n2)&&(n==BAOSHI_COLORFUL||n1==BAOSHI_COLORFUL||n2==BAOSHI_COLORFUL)))
		{
			flag=true;
			Baoshi[p.x][p.y].willdelete=true;
			Baoshi[p1.x][p1.y].willdelete=true;
			Baoshi[p2.x][p2.y].willdelete=true;
		}
	}
/////////////////////////////////////
	p1=p2=p=point;
	p1.x+=1;
	p2.x-=1;
	n=Baoshi[p.x][p.y].color;
	n1=Baoshi[p1.x][p1.y].color;
	n2=Baoshi[p2.x][p2.y].color;
	
	if (p1.x<8&&p2.x>=0)
	{
		if((n==n1&&n==n2&&n1==n2)||((n==n1||n==n2||n1==n2)&&(n==BAOSHI_COLORFUL||n1==BAOSHI_COLORFUL||n2==BAOSHI_COLORFUL)))
		{
			flag=true;
			Baoshi[p.x][p.y].willdelete=true;
			Baoshi[p1.x][p1.y].willdelete=true;
			Baoshi[p2.x][p2.y].willdelete=true;
		}
	}
	///////////////////////////////
	return flag;
}

void CBaoshi::Start()
{
	srand(time(NULL));
	int i,j;
	for (i=0;i<8;i++)
		for(j=0;j<8;j++)
		{
			Baoshi[i][j].color=rand()%8;
			Baoshi[i][j].willdelete=false;
	}
		m_old.x=-1;
		m_old.y=-1;
		m_new.x=-1;
		m_new.y=-1;
	while(Check())
	{
		Delete();
	}

}
