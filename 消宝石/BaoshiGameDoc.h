// BaoshiGameDoc.h : interface of the CBaoshiGameDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BAOSHIGAMEDOC_H__F1BAC5AA_B4C3_4E13_8DD6_7A10512DE2E7__INCLUDED_)
#define AFX_BAOSHIGAMEDOC_H__F1BAC5AA_B4C3_4E13_8DD6_7A10512DE2E7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CBaoshiGameDoc : public CDocument
{
protected: // create from serialization only
	CBaoshiGameDoc();
	DECLARE_DYNCREATE(CBaoshiGameDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaoshiGameDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBaoshiGameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBaoshiGameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BAOSHIGAMEDOC_H__F1BAC5AA_B4C3_4E13_8DD6_7A10512DE2E7__INCLUDED_)
