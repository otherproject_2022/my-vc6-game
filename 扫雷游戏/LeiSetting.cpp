// LeiSetting.cpp : implementation file
//

#include "stdafx.h"
#include "Lei.h"
#include "LeiSetting.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLeiSetting dialog


CLeiSetting::CLeiSetting(CWnd* pParent /*=NULL*/)
	: CDialog(CLeiSetting::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLeiSetting)
	m_High = 0;
	m_Wide = 0;
	m_Num = 0;
	//}}AFX_DATA_INIT
}


void CLeiSetting::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLeiSetting)
	DDX_Text(pDX, IDC_EDIT_HIGH, m_High);
	DDV_MinMaxInt(pDX, m_High, 1, 100);
	DDX_Text(pDX, IDC_EDIT_WIDE, m_Wide);
	DDV_MinMaxInt(pDX, m_Wide, 1, 100);
	DDX_Text(pDX, IDC_EDIT_NUM, m_Num);
	DDV_MinMaxInt(pDX, m_Num, 1, 999);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLeiSetting, CDialog)
	//{{AFX_MSG_MAP(CLeiSetting)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLeiSetting message handlers

void CLeiSetting::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
		if(m_Num>=m_High*m_Wide)
		{
			AfxMessageBox("���벻�Ϸ�!");
			return;
		}
	
	CDialog::OnOK();
}
