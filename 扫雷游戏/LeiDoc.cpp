// LeiDoc.cpp : implementation of the CLeiDoc class
//

#include "stdafx.h"
#include "Lei.h"

#include "LeiDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLeiDoc

IMPLEMENT_DYNCREATE(CLeiDoc, CDocument)

BEGIN_MESSAGE_MAP(CLeiDoc, CDocument)
	//{{AFX_MSG_MAP(CLeiDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLeiDoc construction/destruction

CLeiDoc::CLeiDoc()
{
	// TODO: add one-time construction code here

}

CLeiDoc::~CLeiDoc()
{
}

BOOL CLeiDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CLeiDoc serialization

void CLeiDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CLeiDoc diagnostics

#ifdef _DEBUG
void CLeiDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLeiDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLeiDoc commands
