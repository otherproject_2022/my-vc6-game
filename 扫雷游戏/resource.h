//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Lei.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_LEITYPE                     129
#define IDB_BITMAPFACE                  130
#define IDB_BITMAPSTAT                  131
#define IDB_BITMAPNUM                   132
#define IDD_DIALOG_LEISETTING           133
#define IDC_EDIT_HIGH                   1000
#define IDC_EDIT_WIDE                   1001
#define IDC_EDIT_NUM                    1002
#define ID_GAMESTAR                     32771
#define ID_GAMESET                      32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
