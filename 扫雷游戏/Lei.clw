; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CLeiView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Lei.h"
LastPage=0

ClassCount=6
Class1=CLeiApp
Class2=CLeiDoc
Class3=CLeiView
Class4=CMainFrame

ResourceCount=3
Resource1=IDD_ABOUTBOX
Class5=CAboutDlg
Resource2=IDR_MAINFRAME
Class6=CLeiSetting
Resource3=IDD_DIALOG_LEISETTING

[CLS:CLeiApp]
Type=0
HeaderFile=Lei.h
ImplementationFile=Lei.cpp
Filter=N

[CLS:CLeiDoc]
Type=0
HeaderFile=LeiDoc.h
ImplementationFile=LeiDoc.cpp
Filter=N

[CLS:CLeiView]
Type=0
HeaderFile=LeiView.h
ImplementationFile=LeiView.cpp
Filter=C
BaseClass=CView
VirtualFilter=VWC
LastObject=ID_GAMESET


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=CMainFrame




[CLS:CAboutDlg]
Type=0
HeaderFile=Lei.cpp
ImplementationFile=Lei.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_VIEW_TOOLBAR
Command15=ID_VIEW_STATUS_BAR
Command16=ID_APP_ABOUT
Command17=ID_GAMESTAR
Command18=ID_GAMESET
CommandCount=18

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[DLG:IDD_DIALOG_LEISETTING]
Type=1
Class=CLeiSetting
ControlCount=8
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT_HIGH,edit,1350631552
Control7=IDC_EDIT_WIDE,edit,1350631552
Control8=IDC_EDIT_NUM,edit,1350631552

[CLS:CLeiSetting]
Type=0
HeaderFile=LeiSetting.h
ImplementationFile=LeiSetting.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CLeiSetting

