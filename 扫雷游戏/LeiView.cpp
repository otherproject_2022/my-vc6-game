// LeiView.cpp : implementation of the CLeiView class
//

#include "stdafx.h"
#include "Lei.h"

#include "LeiDoc.h"
#include "LeiView.h"

#include "LeiSetting.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLeiView

IMPLEMENT_DYNCREATE(CLeiView, CView)

BEGIN_MESSAGE_MAP(CLeiView, CView)
	//{{AFX_MSG_MAP(CLeiView)
	ON_COMMAND(ID_GAMESTAR, OnGamestar)
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_GAMESET, OnGameset)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLeiView construction/destruction

CLeiView::CLeiView()
{
	// TODO: add construction code here
	m_BitmapStat.LoadBitmap(IDB_BITMAPSTAT);
	m_BitmapNum.LoadBitmap(IDB_BITMAPNUM);
	m_BitmapFace.LoadBitmap(IDB_BITMAPFACE);
	defcol=14;
	defrow=23;
	defleinum=11;
	gamestart=1;
	jieshu=0;
	Start();

}

CLeiView::~CLeiView()
{
}

BOOL CLeiView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CLeiView drawing

void CLeiView::OnDraw(CDC* pDC)
{
	CLeiDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CBrush mybrush1;
	mybrush1.CreateSolidBrush(RGB(192,192,192));
	CRect myrect1(0,0,1200,800);
	pDC->FillRect(myrect1,&mybrush1);

	CBrush mybrush2;
	mybrush2.CreateSolidBrush(RGB(128,128,128));
	CRect myrect2(0,0,400,45);
	pDC->FillRect(myrect2,&mybrush2);

	CDC Dc;
	if (Dc.CreateCompatibleDC(pDC)==FALSE)
	{
		AfxMessageBox("Can't create DC");
	}

		DrawFace(facenum);


	Dc.SelectObject(m_BitmapStat);
	for (int a=0;a<m_RowCount;a++)
		for (int b=0;b<m_ColCount;b++)
		{
			//weitu=1已按下的数字区		
			//weitu=2显示旗			
			//weitu=3显示问号	
			if(lei[a][b].weitu==0)
				pDC->BitBlt(a*16+10,b*16+50,16,16,&Dc,0,0,SRCCOPY);
			if (lei[a][b].weitu==1)
			{
				pDC->BitBlt(a*16+10,b*16+50,16,16,&Dc,0,256-16-16*lei[a][b].shumu,SRCCOPY);
			}
			if (lei[a][b].weitu==2)
			{
				pDC->BitBlt(a*16+10,b*16+50,16,16,&Dc,0,16,SRCCOPY);
			}
			if (lei[a][b].weitu==3)
			{
				pDC->BitBlt(a*16+10,b*16+50,16,16,&Dc,0,32,SRCCOPY);
			}
			if (jieshu==1&&lei[a][b].shumu==-1)
			{
				pDC->BitBlt(a*16+10,b*16+50,16,16,&Dc,0,80,SRCCOPY);
			}
			if (jieshu==1&&lei[a][b].weitu==2&&lei[a][b].shumu!=-1)
			{
				pDC->BitBlt(a*16+10,b*16+50,16,16,&Dc,0,64,SRCCOPY);
			}
		
		}
	Dc.SelectObject(m_BitmapNum);

	int numbera,numberb,numberc;
	numbera=leftnum/100;
	numberb=leftnum%100/10;
	numberc=leftnum%10;
	int timea,timeb,timec;
	timea=second/100;
	timeb=second%100/10;
	timec=second%10;
	pDC->BitBlt(20,10,13,23,&Dc,0,276-23-23*numbera,SRCCOPY);
	pDC->BitBlt(33,10,13,23,&Dc,0,276-23-23*numberb,SRCCOPY);
	pDC->BitBlt(46,10,13,23,&Dc,0,276-23-23*numberc,SRCCOPY);
	pDC->BitBlt(325,10,13,23,&Dc,0,276-23-23*timea,SRCCOPY);
	pDC->BitBlt(338,10,13,23,&Dc,0,276-23-23*timeb,SRCCOPY);
	pDC->BitBlt(351,10,13,23,&Dc,0,276-23-23*timec,SRCCOPY);
	Dc.DeleteDC();
}

/////////////////////////////////////////////////////////////////////////////
// CLeiView printing

BOOL CLeiView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CLeiView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CLeiView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CLeiView diagnostics

#ifdef _DEBUG
void CLeiView::AssertValid() const
{
	CView::AssertValid();
}

void CLeiView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CLeiDoc* CLeiView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLeiDoc)));
	return (CLeiDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLeiView message handlers

void CLeiView::OnGamestar() 
{
	// TODO: Add your command handler code here
	Start();
	Invalidate();

	
}

void CLeiView::Start()
{
//	GetClientRect();
//	int a1=100,b1=100;
//	SendMessage(WM_SIZE,&a1,&b1);

	if (gamestart==0&&jieshu==0)
	{
		KillTimer(1);
	}
	second=0;
	facenum=4;
	gamestart=1;
	
	m_RowCount=defrow;//列数
	m_ColCount=defcol;//行数
	
	leinum=defleinum;
	leftnum=leinum;
	
	leftunclicknum=m_RowCount*m_ColCount;	
	jieshu=0;	
	int aa=0;
	
	for (int i=0;i<m_RowCount;i++)
	{
		for (int j=0;j<m_ColCount;j++)
		{
			lei[i][j].shumu=0;
			lei[i][j].weitu=0;
		}
	}
	CTime time=GetCurrentTime();
	int s=time.GetSecond();
	do 
	{
		int k=(rand()*s)%m_RowCount;
		int l=(rand()*s)%m_ColCount;
		if(lei[k][l].shumu!=-1)
		{
			lei[k][l].shumu=-1;
			aa++;
		}
	} while (aa<leinum);
	
	for (int a=0;a<m_RowCount;a++)
		for (int b=0;b<m_ColCount;b++)
			if(lei[a][b].shumu==0)
			{
				for (int c=a-1;c<a+2;c++)
					for(int d=b-1;d<b+2;d++)
					{
						if (c>=0&&c<m_RowCount&&d>=0&&d<m_ColCount)
						{
							if(lei[c][d].shumu==-1)
								lei[a][b].shumu++;
						}
					}
			}

}

void CLeiView::DrawFace(int i)
{
	CDC *pDC=GetDC();
	CDC Dc;
	Dc.CreateCompatibleDC(pDC);
	Dc.SelectObject(m_BitmapFace);
	pDC->BitBlt(180,15,24,24,&Dc,0,24*i,SRCCOPY);	
	Dc.DeleteDC();
}

void CLeiView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	second++;
	CRect myRect(325,10,325+13*3,10+26);
	InvalidateRect(&myRect);
	
	CView::OnTimer(nIDEvent);
}

void CLeiView::CheckGame()
{
	if (leftunclicknum<=leinum)
	{	
		jieshu=1;
		facenum=1;
		Invalidate();
		KillTimer(1);
		AfxMessageBox("你赢了!");
	}
}

void CLeiView::LeiZero(int x, int y)
{
	for (int c=x-1;c<x+2;c++)
		for(int d=y-1;d<y+2;d++)
		{
			if (c>=0&&c<m_RowCount&&d>=0&&d<m_ColCount)
			{
				if (lei[c][d].weitu!=1)
				{				
					lei[c][d].weitu=1;
					leftunclicknum--;
					CRect rect;
					rect.left=c*16+10;
					rect.right=rect.left+16;
					rect.top=d*16+50;
					rect.bottom=rect.top+16;
					InvalidateRect(&rect);
					if(lei[c][d].shumu==0)
					{
						LeiZero(c,d);
					}
				}
			}
		}
}

void CLeiView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if (point.x>180&&point.x<204&&point.y>10&&point.y<34)
	{
			Start();
			Invalidate();
	}
	
	if ((point.x>=10)&&(point.x<=10+16*m_RowCount)&&(point.y>=50)&&(point.y<=50+16*m_ColCount))
	{
		
		if (jieshu==1)
		{
			return;
		}
		if (gamestart==1)
		{
			gamestart=0;
			SetTimer(1,1000,NULL);
		}
		DrawFace(3);
		int a=(point.x-10)/16;
		int b=(point.y-50)/16;
		if (lei[a][b].weitu==1)
		{
			return;
		}
		if (lei[a][b].weitu==0||lei[a][b].weitu==3)
		{
			if (lei[a][b].shumu==-1)
			{
				jieshu=1;
				KillTimer(1);
				facenum=2;
				Invalidate();
				
				AfxMessageBox("你输了!");
			}
			else
			{
				
				lei[a][b].weitu=1;
				leftunclicknum--;
				CRect rect;
				rect.left=a*16+10;
				rect.right=rect.left+16;
				rect.top=b*16+50;
				rect.bottom=rect.top+16;
				InvalidateRect(&rect);
				if (lei[a][b].shumu==0)
				{	
					LeiZero(a,b);
					
				}
				CheckGame();		
			}
		}
	}
	CView::OnLButtonDown(nFlags, point);
}

void CLeiView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if (jieshu!=1)
		DrawFace(4);
	
	CView::OnLButtonUp(nFlags, point);
}

void CLeiView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if (jieshu==1)
	{
		return;
	}

	if ((point.x>=10)&&(point.x<=10+16*m_RowCount)&&(point.y>=50)&&(point.y<=50+16*m_ColCount))
	{
		int a=(point.x-10)/16;
		int b=(point.y-50)/16;
		if (lei[a][b].weitu==0)
		{
			lei[a][b].weitu=2;
			leftnum--;
		}
		else
			if (lei[a][b].weitu==2)
			{
				lei[a][b].weitu=3;
				leftnum++;
			}
			else
				if (lei[a][b].weitu==3)
				{
					lei[a][b].weitu=0;
				}
				
				CRect rect;
				rect.left=20;
				rect.right=rect.left+13*3;
				rect.top=10;
				rect.bottom=rect.top+23;
				InvalidateRect(&rect);
				
				CRect rect2;
				rect2.left=a*16+10;
				rect2.right=rect2.left+16;
				rect2.top=b*16+50;
				rect2.bottom=b*16+66;
				InvalidateRect(&rect2);
	}
	
	CView::OnRButtonDown(nFlags, point);
}

void CLeiView::OnGameset() 
{
	// TODO: Add your command handler code here
	CLeiSetting dlg;
	dlg.m_Wide=defrow;
	dlg.m_High=defcol;
	dlg.m_Num=defleinum;
	if (dlg.DoModal()==IDOK)
	{
		defrow=dlg.m_Wide;
		defcol=dlg.m_High;
		defleinum=dlg.m_Num;
		Start();
		Invalidate();
	}
	
}
