#if !defined(AFX_LEISETTING_H__8D2F9D44_BE26_4718_9801_E2CCFFA03B47__INCLUDED_)
#define AFX_LEISETTING_H__8D2F9D44_BE26_4718_9801_E2CCFFA03B47__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LeiSetting.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLeiSetting dialog

class CLeiSetting : public CDialog
{
// Construction
public:
	CLeiSetting(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLeiSetting)
	enum { IDD = IDD_DIALOG_LEISETTING };
	int		m_High;
	int		m_Wide;
	int		m_Num;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLeiSetting)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLeiSetting)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEISETTING_H__8D2F9D44_BE26_4718_9801_E2CCFFA03B47__INCLUDED_)
