// Lei.h : main header file for the LEI application
//

#if !defined(AFX_LEI_H__6D3021BC_217B_44D3_81BE_3B717A20769D__INCLUDED_)
#define AFX_LEI_H__6D3021BC_217B_44D3_81BE_3B717A20769D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CLeiApp:
// See Lei.cpp for the implementation of this class
//

class CLeiApp : public CWinApp
{
public:
	CLeiApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLeiApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CLeiApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEI_H__6D3021BC_217B_44D3_81BE_3B717A20769D__INCLUDED_)
