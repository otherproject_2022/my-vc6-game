// LeiView.h : interface of the CLeiView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEIVIEW_H__EB4024E6_1DF9_44F3_AF25_54B8750E109D__INCLUDED_)
#define AFX_LEIVIEW_H__EB4024E6_1DF9_44F3_AF25_54B8750E109D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Lei
{
public:
	int weitu;
	int shumu;
};

class CLeiView : public CView
{
protected: // create from serialization only
	CLeiView();
	DECLARE_DYNCREATE(CLeiView)

// Attributes
public:
	CLeiDoc* GetDocument();

// Operations
public:

	int defleinum;
	int defcol;
	int defrow;
	
	int facenum;
	int leftnum;
	int leinum;
	int jieshu;
	int gamestart;
	int second;
	CBitmap m_BitmapStat;
	CBitmap m_BitmapFace;
	CBitmap m_BitmapNum;
	int m_RowCount;
	int m_ColCount;
	Lei lei[50][50];
	int leftunclicknum;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLeiView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	void LeiZero(int x,int y);
	void CheckGame();
	void DrawFace(int i=1);
	void Start();
	virtual ~CLeiView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLeiView)
	afx_msg void OnGamestar();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnGameset();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in LeiView.cpp
inline CLeiDoc* CLeiView::GetDocument()
   { return (CLeiDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEIVIEW_H__EB4024E6_1DF9_44F3_AF25_54B8750E109D__INCLUDED_)
