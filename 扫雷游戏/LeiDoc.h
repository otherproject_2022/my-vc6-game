// LeiDoc.h : interface of the CLeiDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEIDOC_H__E020BA6C_B164_4999_BB98_ACBEF58403EC__INCLUDED_)
#define AFX_LEIDOC_H__E020BA6C_B164_4999_BB98_ACBEF58403EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CLeiDoc : public CDocument
{
protected: // create from serialization only
	CLeiDoc();
	DECLARE_DYNCREATE(CLeiDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLeiDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLeiDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLeiDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEIDOC_H__E020BA6C_B164_4999_BB98_ACBEF58403EC__INCLUDED_)
